#include "Game.h"
#include <GLES3/gl3.h>
#include <Engine/Log.h>
#include <cmath>
#include <Engine/Clock.h>

Game::Game():
m_renderer()
{

}
Game::~Game()
{

}

void Game::Load()
{
    m_renderer.reset(new SpriteRenderer());
    m_shader.reset(new Shader("simple.Shader"));
}
void Game::FixedUpdate()
{
}
void Game::Update()
{
}
void Game::Render()
{
    (*m_shader).Bind();
    (*m_shader).SetUniform4f("u_colour", glm::vec4(std::sin(Clock::GetInstance().GetTotalTime()) * 0.5f + 0.5f, std::cos(Clock::GetInstance().GetTotalTime()* 0.25f) * 0.5f + 0.5f, 1.0f - std::sin(Clock::GetInstance().GetTotalTime() * 0.5f) * 0.5f + 0.5f, 1.0f));
    (*m_renderer).Render();
    Shader::UnBind();
}
void Game::UnLoad()
{
}
#include <Game.h>
#include <memory>

void android_main(struct android_app* a_state)
{
    std::unique_ptr<Game> game(new Game());
    (*game).Run(a_state, 512, 288);
}
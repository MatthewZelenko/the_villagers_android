cmake_minimum_required(VERSION 3.4.1)

set(GAME_SOURCE
    ${SOURCE_DIR}/Game/Main.cpp
    ${SOURCE_DIR}/Game/Game.cpp)


add_library(game-lib
             SHARED
             ${GAME_SOURCE})




target_include_directories(game-lib
                            PRIVATE
                            ${CMAKE_CURRENT_SOURCE_DIR}/..
                            ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(game-lib
                        engine-lib
                        native_app_glue
                        android
                        EGL
                        GLESv3
                        log
                        glm-lib)


message("----------------------------GAME--------------------------")
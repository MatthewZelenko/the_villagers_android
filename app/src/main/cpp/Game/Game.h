#ifndef THEVILLAGERS_GAME_H
#define THEVILLAGERS_GAME_H

#include "Engine/Application.h"
#include "Engine/SpriteRenderer.h"
#include <memory>
#include "Engine/Shader.h"

class Game : public Application
{
public:
    Game();
    ~Game();

protected:
    void Load() override;
    void UnLoad() override;

    void FixedUpdate() override;
    void Update() override;
    void Render() override;

private:
    std::unique_ptr<SpriteRenderer> m_renderer;
    std::unique_ptr<Shader> m_shader;

};
#endif //THEVILLAGERS_GAME_H
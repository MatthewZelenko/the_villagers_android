#ifndef THEVILLAGERS_TIME_H
#define THEVILLAGERS_TIME_H

#include <sys/time.h>

class Application;

class Clock
{
public:
    Clock();

    static Clock& GetInstance()
    {
        static Clock instance;
        return instance;
    }

    double GetDeltaTime() { return m_deltaTime; }
    double GetTotalTime() { return m_elapsedTime; }
    double GetTimeStep() { return m_timeStep; }

private:


    timeval m_systemTime;

    double m_deltaTime, m_elapsedTime;
    double m_elapsedTimeStep, m_timeStep;

    void Init();
    void Update();

    friend Application;
};


#endif //THEVILLAGERS_TIME_H

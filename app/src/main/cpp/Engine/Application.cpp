#include "Application.h"
#include "Clock.h"
#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include "Log.h"
#include "Input.h"

android_app* Application::m_androidState = nullptr;

Application::Application() :
        m_display(EGL_NO_DISPLAY), m_context(EGL_NO_CONTEXT), m_surface(EGL_NO_SURFACE),
        m_isApplicationReady(false)
{

}

Application::~Application()
{
}

void Application::Run(android_app *a_app, int a_gameWidth, int a_gameHeight)
{
    Application::m_androidState = a_app;
    a_app->userData = this;
    a_app->onAppCmd = OnAppCmd;
    a_app->onInputEvent = OnInputEvent;

    m_window.m_gameWidth = a_gameWidth;
    m_window.m_gameHeight = a_gameHeight;

    int32_t events;
    android_poll_source *source;
    //Log::Info("Main Loop Started");
    while (true)
    {
        while (ALooper_pollAll(0, NULL, &events, (void **) &source) >= 0)
        {
            if (source != NULL)
            {
                source->process(m_androidState, source);
            }
            if (m_androidState->destroyRequested)
            {
                return;
            }
        }

        if (IsApplicationReady())
        {
            SystemUpdate();\
            //TODO: Let subclass to control clear and swap.
            glClear(GL_COLOR_BUFFER_BIT);
            Render();
            eglSwapBuffers(m_display, m_surface);

            SystemLateUpdate();
        }
    }
}
bool Application::CreateDisplay()
{
    const EGLint attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_DEPTH_SIZE, 24,
            EGL_NONE
    };
    EGLint format = 0;
    EGLint numConfigs = 0;
    EGLConfig config = NULL;

    //Get the display and init with it
    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    eglInitialize(m_display, NULL, NULL);

    //Find the best configs that match the attribs
    eglChooseConfig(m_display, attribs, nullptr, 0, &numConfigs);
    if(!numConfigs)
    {
        Log::Error("Could not find appropriate configs");
        return false;
    }

    EGLConfig* supportedConfigs = new EGLConfig[numConfigs];
    assert(supportedConfigs);

    //Get those configs
    eglChooseConfig(m_display, attribs, supportedConfigs, numConfigs, &numConfigs);
    assert(numConfigs);

    //Iterate through all the configs and find the matching one
    int i = 0;
    for (; i < numConfigs; ++i)
    {
        EGLConfig& cfg = supportedConfigs[i];
        EGLint r, g, b, d;
        if(eglGetConfigAttrib(m_display, cfg, EGL_RED_SIZE, &r) &&
           eglGetConfigAttrib(m_display, cfg, EGL_GREEN_SIZE, &g) &&
           eglGetConfigAttrib(m_display, cfg, EGL_BLUE_SIZE, &b) &&
           eglGetConfigAttrib(m_display, cfg, EGL_DEPTH_SIZE, &d) &&
           r == 8 && g == 8 && b == 8 && d == 24)
        {
            config = cfg;
            break;
        }
    }

    if(i == numConfigs)
    {
        config = supportedConfigs[0];
    }

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    eglGetConfigAttrib(m_display, config, EGL_NATIVE_VISUAL_ID, &format);
    ANativeWindow_setBuffersGeometry(m_androidState->window, 0, 0, format);


    //create a new EGL window surface
    m_surface = eglCreateWindowSurface(m_display, config, m_androidState->window, NULL);

    //create a new EGL rendering context
    const EGLint contextAttribs[] = {
            EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE
    };
    m_context = eglCreateContext(m_display, config, EGL_NO_CONTEXT, contextAttribs);

    if (eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_FALSE)
    {
        Log::Error("Could not make context current");
        return false;
    }

    //Query the width and height of the surface in pixels
    eglQuerySurface(m_display, m_surface, EGL_WIDTH, &m_window.m_displayWidth);
    eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &m_window.m_displayHeight);
    m_window.m_displayRatio = (float)m_window.m_displayWidth / m_window.m_displayHeight;

    //GL
    glDisable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
    eglSwapInterval(m_display, 1);
    return true;
}
void Application::DestroyDisplay()
{
    if (m_display != EGL_NO_DISPLAY)
    {
        eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (m_context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(m_display, m_context);
            m_context = EGL_NO_CONTEXT;
        }
        if (m_surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(m_display, m_surface);
            m_surface = EGL_NO_SURFACE;
        }
        eglTerminate(m_display);
        m_display = EGL_NO_DISPLAY;
    }
}
void Application::SystemInit()
{
    CreateDisplay();
    Log::Debug("%f", m_window.m_displayRatio);
    Log::Debug("%d", m_window.m_displayWidth);
    Log::Debug("%d", m_window.m_displayHeight);


    AConfiguration_setOrientation(m_androidState->config, ACONFIGURATION_ORIENTATION_LAND);
    m_window.CalculateRatios();
    Clock::GetInstance().Init();
    Input::GetInstance().SetScreenSize(m_window.m_displayWidth, m_window.m_displayHeight);

    Load();
    m_isApplicationReady = true;
}
void Application::SystemUpdate()
{
    Clock::GetInstance().Update();
    Input::GetInstance().Update();
    Update();
}
void Application::SystemDestroy()
{
    UnLoad();
    DestroyDisplay();
    m_isApplicationReady = false;
}
void Application::OnAppCmd(android_app *a_app, int32_t a_cmd)
{
    Application* app = (Application *) a_app->userData;
    switch (a_cmd)
    {
        case APP_CMD_INIT_WINDOW:
        {
            app->SystemInit();
            break;
        }
        case APP_CMD_TERM_WINDOW:
        {
            app->SystemDestroy();
            break;
        }
        case APP_CMD_START:
        case APP_CMD_RESUME:
        case APP_CMD_PAUSE:
        case APP_CMD_STOP:
        default:
            break;
    }
}
int32_t Application::OnInputEvent(android_app *a_app, AInputEvent *a_event)
{
    int32_t eventType = AInputEvent_getType(a_event);
    switch (eventType)
    {
        case AINPUT_EVENT_TYPE_MOTION:
        {
            switch (AInputEvent_getSource(a_event))
            {
                case AINPUT_SOURCE_TOUCHSCREEN:
                {
                    int action = AKeyEvent_getAction(a_event) & AMOTION_EVENT_ACTION_MASK;
                    switch (action)
                    {
                        case AMOTION_EVENT_ACTION_DOWN:
                        {
                            Input::GetInstance().OnActionDown(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            return 1;
                        }
                        case AMOTION_EVENT_ACTION_UP:
                        {
                            Input::GetInstance().OnActionUp(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            return 1;
                        }
                        case AMOTION_EVENT_ACTION_MOVE:
                        {
                            Input::GetInstance().OnActionMove(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            return 1;
                        }
                        default:
                            break;
                    }
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    return 0;
}
void Application::Quit()
{
    ANativeActivity_finish(m_androidState->activity);
}

void Application::SystemLateUpdate()
{
    Input::GetInstance().LateUpdate();
}

Window::Window():
    m_gameWidth(0), m_gameHeight(0),
    m_displayWidth(0), m_displayHeight(0)
{


}

void Window::CalculateRatios()
{
    m_gameRatio = (float)m_gameWidth / (float)m_gameHeight;

//    if (m_gameRatio < m_displayRatio)
//    {
//        float newHeight = m_displayWidth / m_gameRatio;
//
//        float heightDiff = newHeight - m_displayHeight;
//        m_yDisplayExt = heightDiff * 0.5f;
//        glViewport(0, -m_yDisplayExt, m_displayWidth, m_displayHeight + heightDiff);
//
//        m_window.m_yGameExt = m_yDisplayExt / newHeight * (float)m_window.m_gameHeight;
//    }
//    else
//    {
//        float newWidth = m_window.m_displayHeight * ratio;
//
//        float widthDiff = newWidth - m_window.m_displayWidth;
//        m_xDisplayExt = widthDiff * 0.5f;
//        glViewport(-m_xDisplayExt, 0, m_window.m_displayWidth + widthDiff, m_window.m_displayHeight);
//
//        m_window.m_xGameExt = m_xDisplayExt / newWidth * (float)m_window.m_gameWidth;
//    }

}

#ifndef THEVILLAGERS_SHADER_H
#define THEVILLAGERS_SHADER_H

#include <GLES3/gl3.h>
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

struct ShaderProgramSource;

class Shader
{
public:
    Shader(const std::string& a_shaderPath);
    ~Shader();

    void Bind();
    static void UnBind();

    void SetUniform1f(const std::string& a_name, float a_value);
    void SetUniform2f(const std::string& a_name, glm::vec2 a_value);
    void SetUniform3f(const std::string& a_name, glm::vec3 a_value);
    void SetUniform4f(const std::string& a_name, glm::vec4 a_value);

private:
    unsigned int m_id;
    std::unordered_map<std::string, int> m_uniformLocations;

    void ReadShaderFromFile(const std::string& a_shaderPath);
    ShaderProgramSource ParseShader(const std::string& a_string);
    int CompileShader(unsigned int a_shaderType, const std::string& a_source);
    void LinkProgram(const int*a_ids);


    int Getlocation(const std::string& a_name);

};


#endif //THEVILLAGERS_SHADER_H
#ifndef THEVILLAGERS_SPRITERENDERER_H
#define THEVILLAGERS_SPRITERENDERER_H


class SpriteRenderer
{
public:
    SpriteRenderer();
    ~SpriteRenderer();

    void Render();

private:
    unsigned int m_vao, m_vbo, m_ebo;

};


#endif //THEVILLAGERS_SPRITERENDERER_H

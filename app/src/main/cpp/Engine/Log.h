#ifndef THEVILLAGERS_LOG_H
#define THEVILLAGERS_LOG_H
#include <string>

namespace Log
{
    void Info(const char* a_message, ...);
    void Error(const char* a_message, ...);
    void Warning(const char* a_message, ...);
    void Debug(const char* a_message, ...);
    std::string IntToString(int a_value);
}

#endif //THEVILLAGERS_LOG_H

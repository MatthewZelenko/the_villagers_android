#include "Input.h"
#include <glm/glm.hpp>

Input::Input() :
        m_screenHeight(nullptr), m_screenWidth(nullptr),
        m_touchState(TouchState::TOUCH_UP),
        m_deadZoneSqr(0.0f),
        m_isDirty(true)
{

}

Input::~Input()
{
}

Input& Input::GetInstance()
{
    static Input instance;
    return instance;
}

void Input::Update()
{
    if(m_isDirty)
    {
        if (m_touchState <= TouchState::TOUCH_DOWN)
        {
            m_delta = m_currentPosition - m_startPosition;
            m_deltaFromStart = m_lastPosition - m_currentPosition;
        }
    }
}
void Input::LateUpdate()
{
    if (m_touchState == TouchState::TOUCH_PRESSED)
    {
        m_touchState = TouchState::TOUCH_DOWN;
        m_isDirty = true;
    }
    else if (m_touchState == TouchState::TOUCH_RELEASED)
    {
        m_touchState = TouchState::TOUCH_UP;
        m_isDirty = true;
    }

    if (m_isDirty)
    {
        if (m_touchState <= TouchState::TOUCH_RELEASED)
        {
            m_lastPosition = m_currentPosition;
        }
        else
        {
            if (m_touchState == TouchState::TOUCH_UP)
            {
                m_delta = m_deltaFromStart = m_startPosition = m_currentPosition = m_lastPosition = glm::vec2();
            }
        }
        m_isDirty = false;
    }
}

void Input::OnActionMove(float a_x, float a_y)
{
    a_y = *m_screenHeight - a_y;
    m_currentPosition.x = a_x;
    m_currentPosition.x = a_y;
    m_isDirty = true;

}
void Input::OnActionUp(float a_x, float a_y)
{
    m_touchState = TouchState::TOUCH_RELEASED;

    a_y = *m_screenHeight - a_y;
    m_currentPosition.x = a_x;
    m_currentPosition.x = a_y;
    m_isDirty = true;

}
void Input::OnActionDown(float a_x, float a_y)
{
    m_touchState = TouchState::TOUCH_PRESSED;

    a_y = *m_screenHeight - a_y;
    m_startPosition.x = m_currentPosition.x = a_x;
    m_startPosition.y = m_currentPosition.x = a_y;
    m_isDirty = true;
}
void Input::SetScreenSize(const int &a_width, const int &a_height)
{
    m_screenWidth = &a_width;
    m_screenHeight = &a_height;
}

glm::vec2 Input::DeltaFromStart()
{
    float length = (m_deltaFromStart.x * m_deltaFromStart.x) + (m_deltaFromStart.y * m_deltaFromStart.y);

    if(length >= m_deadZoneSqr)
        return m_deltaFromStart;
    return glm::vec2();
}

glm::vec2 Input::Delta()
{
    if(m_touchState > TouchState::TOUCH_DOWN)return glm::vec2();

    float length = (m_delta.x * m_delta.x) + (m_delta.y * m_delta.y);

    if(length >= m_deadZoneSqr)
        return m_delta;
    return glm::vec2();
}
#include "SpriteRenderer.h"
#include <GLES3/gl3.h>

SpriteRenderer::SpriteRenderer()
{
    float verts[] = {
            -0.5f, -0.5f,       //BOTTOM_LEFT
            -0.5f, 0.5f,        //TOP_LEFT
            0.5f, -0.5f,        //BOTTOM_RIGHT
            0.5f, 0.5f          //TOP_RIGHT
    };
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const void*)0);


    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
SpriteRenderer::~SpriteRenderer()
{
    glDeleteBuffers(1, &m_ebo);
    glDeleteBuffers(1, &m_vbo);
    glDeleteBuffers(1, &m_vao);
}

void SpriteRenderer::Render()
{
    glBindVertexArray(m_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

#ifndef THEVILLAGERS_INPUT_H
#define THEVILLAGERS_INPUT_H

#include <glm/vec2.hpp>

class Application;
class Input
{
public:
    Input();
    ~Input();

    static Input& GetInstance();

    inline bool TouchPressed()
    {
        return m_touchState == TouchState::TOUCH_PRESSED;
    }
    inline bool TouchDown()
    {
        return m_touchState == TouchState::TOUCH_DOWN;
    }
    inline bool TouchReleased()
    {
        return m_touchState == TouchState::TOUCH_RELEASED;
    }
    inline bool TouchUp()
    {
        return m_touchState == TouchState::TOUCH_UP;
    }

    //Returns dragged distance from the start;
    glm::vec2 DeltaFromStart();
    //Returns dragged distance from the last frame;
    glm::vec2 Delta();

    inline glm::vec2 GetStartPosition()     {return m_startPosition;}
    inline glm::vec2 GetLastPosition()      {return m_lastPosition;}
    inline glm::vec2 GetCurrentPosition()   {return m_currentPosition;}


private:
    friend class Application;

    void Update();
    void LateUpdate();
    void OnActionDown(float a_x, float a_y);
    void OnActionUp(float a_x, float a_y);
    void OnActionMove(float a_x, float a_y);
    void SetScreenSize(const int& a_width, const int& a_height);


    const int* m_screenWidth, *m_screenHeight;
    glm::vec2 m_startPosition, m_currentPosition, m_lastPosition;
    float m_deadZoneSqr;

    glm::vec2 m_delta, m_deltaFromStart;


    bool m_isDirty;


    enum class TouchState : int
    {
        TOUCH_PRESSED = 0,
        TOUCH_DOWN = 1,
        TOUCH_RELEASED = 2,
        TOUCH_UP = 3
    };
    TouchState m_touchState;
};

#endif //THEVILLAGERS_INPUT_H

#ifndef THEVILLAGERS_APPLICATION_H
#define THEVILLAGERS_APPLICATION_H

#include "android_native_app_glue.h"
#include <EGL/egl.h>

class Application;

class Window
{
public:
    Window();

    int GetDisplayWidth(){return m_displayWidth;}
    int GetDisplayHeight(){return m_displayHeight;}
    int GetGameWidth(){return m_gameWidth;}
    int GetGameHeight(){return m_gameHeight;}

private:
    friend Application;

    void CalculateRatios();

    float m_displayRatio, m_gameRatio;

    int m_displayWidth, m_displayHeight;
    int m_gameWidth, m_gameHeight;
};

class Application
{
public:
    Application();

    virtual ~Application();

    void Run(android_app* a_app, int a_gameWidth, int a_gameHeight);
    void Quit();

    bool IsApplicationReady(){return m_isApplicationReady;}

    static android_app* m_androidState;
    Window m_window;

protected:
    virtual void Load(){}
    virtual void Update(){}
    virtual void FixedUpdate(){}
    virtual void Render(){}
    virtual void UnLoad(){}

private:
    void SystemInit();
    void SystemUpdate();
    void SystemLateUpdate();
    void SystemDestroy();

    //Creates and OpenGL context from device
    bool CreateDisplay();
    void DestroyDisplay();

    static void OnAppCmd(android_app* a_app, int32_t a_cmd);
    static int32_t OnInputEvent(android_app* a_app, AInputEvent* a_event);


    EGLDisplay m_display;
    EGLSurface m_surface;
    EGLContext m_context;

    bool m_isApplicationReady;
};


#endif //THEVILLAGERS_APPLICATION_H
